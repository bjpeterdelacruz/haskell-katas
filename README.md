# Haskell Katas

Katas are programming exercises designed to help programmers improve their skills through practice and repetition.

#### Profiles

- [Developer's Website](https://bjdelacruz.dev)
- [Coderbyte](https://coderbyte.com/profile/bjpeterdelacruz)
- [Codesignal](https://app.codesignal.com/profile/bjpeter)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)